# Estatutos del capítulo estudiantil __UNAM-FI__ de la _Association for Computing Machinery_

## Artículo I: Nombre

1.  El nombre de esta organización es Capítulo Estudiantil de la ACM
    __UNAM-FI__. De ahora en adelante referido como __Capítulo__; la
    _Association for Computing Machinery_ se referirá como _ACM_.

## Artículo II: Propósito

1.  El __Capítulo__ está organizado y funciona exclusivamente con
    fines educativos y científicos, para promover lo siguiente:
    a.  Un mayor conocimiento y un mayor interés en la ciencia, diseño,
        desarrollo, construcción, lenguaje, administración y aplicación
        de computación moderna.
    b.  Mayor interés en computación y sus aplicaciones.
    c.  La comunicación entre personas con un interés en computación.
2.  El __Capítulo__ orientará a los estudiantes de la Facultad de
    Ingeniería de la UNAM y otras personas interesadas de la comunidad.
3.  El __Capítulo__ está colegiado por la _ACM_.

## Artículo III: Membresía

1.  La afiliación al __Capítulo__ está abierta para todo/a miembro o
    no miembro de la _ACM_.
2.  La capacidad de votar en el __Capítulo__ se concede a todos
    los miembros activos del __Capítulo__.
3.  La afiliación no será restringida por raza, creencia, edad,
    género, discapacidad física o mental, nacionalidad, orientación sexual,
    estatus económico o paternidad.

## Artículo IV: Funcionarios

1.  Los funcionarios de este __Capítulo__ serán: Presidente,
    Vicepresidente, y Tesorero. Estas personas deben tener Membresía
    Estudiantil de la _ACM_. Además, se requiere un Docente Promotor
    con una Membresía Profesional de la _ACM_. También es posible
    establecer cargos adicionales.
2.  Los funcionarios deben ser electos por mayoría de votos
    emitidos en una reunión electoral anual o por votación electrónica
    y servirán por un mínimo de un año.
3.  El nombramiento o nominación de una persona para una posición de
    funcionario dentro del __Capítulo__ esta limitado a dos mandatos
    consecutivos en la misma posición. Esta recomendación tiene la
    intención de animar la continua incorporación de nuevos voluntarios
    a la organización y alentar a los funcionarios para que reflexionen
    sobre la planificación de su sucesor.

## Artículo V: Deberes de los Funcionarios

1.  El Presidente es el funcionario principal y responsable
    de dirigir y planear las actividades del __Capítulo__ de acuerdo con las
    politicas y procedimientos dictados por la _ACM_ y estos estatutos.
2.  El Vicepresidente presidirá las reuniones en ausencia de el
    Presidente, ayudará al Presidente en la gestión del __Capítulo__
    y desempeñará las demás funciones que le asigne el Presidente.
3.  El Tesorero guardará las minutas de todas las reuniones del
    __Capítulo__ y mantendrá los registros financieros del __Capítulo__. Otros
    deberes incluyen:
    a.  Cobrar cuotas, pagar las cuentas y mantener los registros
    financieros del __Capítulo__.
    b.  Preparar del informe anual del __Capítulo__ y mantener actualizada
    la información de contacto de los funcionarios, y presentar ambos a
    la ACM a través de la Interfaz Administrativa del __Capítulo__.
    c. Desempeñar las demás funciones que le asigne el Presidente.
4.  El Docente Promotor será un miembro de la facultad o un miembro del
    personal de tiempo completo de su escuela. Cada Capítulo Estudiantil
    tiene un Promotor.
	a.  El Docente Promotor debe ser un miembro con derecho a voto de
	la ACM y un miembro de la facultad o personal de tiempo completo
	de la Facultad de Ingeniería de la Universidad Nacional Autónoma
	de México.
    b.  El Docente Promotor será generalmente responsable de
    las actividades del __Capítulo__. Específicamente, el Promotor:
        i.  Ayuda a proporcionar continuidad a el __Capítulo__ a medida
        que el liderazgo estudiantil y el personal cambian.
        ii. Promueve las buenas relaciones entre estudiantes y profesores.
        iii.Ayuda a mantener los estándares universitarios en todas
        las actividades del __Capítulo__.
        iv. Ejerce la supervisión financiera, si es necesario,
        promoviendo el pronto pago de las facturas y el cobro de las
        cuotas, y supervisando la liquidación de todas las cuentas en
        caso de disolución del __Capítulo__.
        v.  Representa los intereses del __Capítulo__ ante el
        profesorado y la administración
        vi. Para los Capítulos en escuelas secundarias y los
        estudiantes menores de edad, el Docente Promotor debe asistir
        a todas las reuniones que se llevan a cabo por la noche. En
        caso de que el patrocinador designado no esté disponible,
        otro miembro del profesorado puede acompañar la reunión. Si
        ambos no están disponibles, un profesional del área local,
        previamente investigado y aprobado por la escuela, puede ser
        asignado para supervisar las reuniones nocturnas.

## Artículo VI: Reuniones

1.  El __Capítulo__ se reunirá sólo en lugares abiertos y accesibles a
    todos los miembros de la Asociación.
2.  Se debe celebrar una reunión electoral anual. En esta reunión,
    el Tesorero presentará los informes necesarios. Asimismo, se
    procederá a la elección de los funcionarios.
3.  Los avisos de todas las reuniones se distribuirán a todos los
    miembros al menos una semana antes de cualquier reunión.

## Artículo VII: Desembolsos y cuotas

1.	Los desembolsos de la Tesorería del __Capítulo__ para gastos, serán
	hechos por cualquier funcionario activo del __Capítulo__ y se incluirán
	en las minutas de sus reuniones.
2.	Las cuotas se fijarán anualmente.

## Artículo VIII: Procedimiento de votación y enmienda

1.	Todos los cambios propuestos a estos Estatutos del __Capítulo__
	deberán ser aprobados por el Departamento de Capítulos de la ACM antes
	de ser presentados a los miembros del __Capítulo__ para su votación.
2.	No se llevarán a cabo asuntos oficiales del __Capítulo__ a menos que
	haya quórum. El quórum del __Capítulo__ se define como la mayoría
	de los miembros votantes del __Capítulo__.
3.	Los funcionarios del __Capítulo__ serán elegidos por la mayoría
    de votos emitidos.

## Artículo IX: Código de Conducta

1.	El acoso o el comportamiento hostil no es bienvenido, incluyendo
	el discurso que intimida, crea incomodidad o interfiere con la
	participación o la oportunidad de participación de una persona
	en una reunión o evento del __Capítulo__. No se tolerará el acoso
	en ninguna forma, incluyendo pero no limitado al acoso basado
	en la condición de extranjero o ciudadanía, edad, color, credo,
	discapacidad, estado civil, estado militar, origen nacional,
	embarazo, condiciones médicas relacionadas con el parto y el
	embarazo, raza, religión, sexo, género, condición de veterano,
	orientación sexual o cualquier otra condición protegida por las
	leyes en las que se celebre la reunión del __Capítulo__ o evento
	del __Capítulo__. El acoso incluye el uso de lenguaje abusivo o
	degradante, intimidación, acecho, acoso fotográfico o
	grabación, contacto físico inapropiado, imágenes sexuales y
	atención sexual no deseada. No se aceptará excusa alguna en
	la que el participante "sólo bromeaba", "estaba jugando" o
	"estaba siendo jugueton".
2.	Cualquier persona que presencie o esté sujeta a un
	comportamiento inaceptable debe notificar a un funcionario del
	__Capítulo__ o a las oficinas centrales de la ACM.
3.	Los individuos que violan estas normas pueden ser sancionados
	o excluidos de participar a discreción de los funcionarios
	del __Capítulo__.

## Artículo X: Disolución del Capítulo

1.	La disolución de este __Capítulo__ por consentimiento de los miembros
	consistirá en el acuerdo unánime de todos sus funcionarios junto con un
	voto mayoritario en una reunión que haya sido publicada con antelación
	a todos los miembros del __Capítulo__ con el fin de tomar este voto.
2.	En caso de disolución de este __Capítulo__, sus activos y pasivos serán
	transferidos a ACM y serán supervisados por el Director Financiero de
	ACM. Los fondos dados al Capítulo por la Universidad serán devueltos
	a la Universidad.

